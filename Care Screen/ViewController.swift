//
//  ViewController.swift
//  Care Screen
//
//  Created by Rishabh  on 14/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var myTableView: UITableView!
    var myTextFieldRectWithRespectToView: CGRect? = nil
    var height:CGFloat! = 200
    var myVar:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerForKeyboardNotifications()
        // Do any additional setup after loading the view, typically from a nib.
        myTableView.registerNib(UINib(nibName: "tableViewCell1", bundle: nil), forCellReuseIdentifier: "TableViewCell1")
      
        myTableView.registerNib(UINib(nibName: "tableViewCell2", bundle: nil), forCellReuseIdentifier: "TableViewCell2")
        myTableView.registerNib(UINib(nibName: "tableViewCell3", bundle: nil), forCellReuseIdentifier: "TableViewCell3")
         myTableView.registerNib(UINib(nibName: "tableViewCell4", bundle: nil), forCellReuseIdentifier: "TableViewCell4")
        myTableView.registerNib(UINib(nibName: "tableViewCell5", bundle: nil), forCellReuseIdentifier: "TableViewCell5")
        myTableView.registerNib(UINib(nibName: "tableViewCell6", bundle: nil), forCellReuseIdentifier: "TableViewCell6")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("updateHeight"), name: "ImageTapped", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("hidePickerView"), name: "RowSelected", object: nil)
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodForPickerViewFinished:"), name: "DatePickerViewFinished", object: nil)
        
            }
    
    func updateHeight() {
        height = 300
        self.myTableView.reloadData()
    }
    
    func hidePickerView() {
        height = 200
        myVar = true
        self.myTableView.reloadData()

    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        if indexPath.row == 0 {
            let  cell = self.myTableView.dequeueReusableCellWithIdentifier("TableViewCell1")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            return cell!
        }
        else if indexPath.row == 1{
            let  cell = self.myTableView.dequeueReusableCellWithIdentifier("TableViewCell2")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            return cell!
        } else if indexPath.row == 2{
            let cell = self.myTableView.dequeueReusableCellWithIdentifier("TableViewCell3")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            return cell!
        } else if indexPath.row == 3{
            let cell = self.myTableView.dequeueReusableCellWithIdentifier("TableViewCell4")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            return cell!
        } else if indexPath.row == 4{
            let cell = self.myTableView.dequeueReusableCellWithIdentifier("TableViewCell5")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            return cell!
        } else {
            let cell = self.myTableView.dequeueReusableCellWithIdentifier("TableViewCell6")
            cell!.selectionStyle = UITableViewCellSelectionStyle.None
            self.myTextFieldRectWithRespectToView = cell!.convertRect((cell as! tableViewCell6).myTextView.frame, toView: self.view)
            return cell!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 243.0
        } else if indexPath.row == 1 {
            return 200.0
        } else if indexPath.row == 2 {
            return 200.0
        } else if indexPath.row == 3 {
            return height
        } else if indexPath.row == 4 {
            return 250.0
        } else if indexPath.row == 5 {
            return 300.0
        }
        return 0
    }
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillShow(_:)),
                                       name: UIKeyboardWillShowNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillHide(_:)),
                                       name: UIKeyboardWillHideNotification,
                                       object: nil)

    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        let userInfo = notification.userInfo
        if let info = userInfo
        {
//            let animationDurationObject =
//                info[UIKeyboardAnimationDurationUserInfoKey] as! NSValue
            
            let keyboardEndRectObject =
                info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            
            let keyboardBoundsWithRespectToView = self.view.convertRect(keyboardEndRectObject.CGRectValue(), fromView: nil)
            let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardBoundsWithRespectToView.size.height, right: 0)
            let scrollable:CGFloat
            if myVar == false {
                scrollable = ((myTextFieldRectWithRespectToView?.origin.y)! + (myTextFieldRectWithRespectToView?.size.height)!)
            } else {
                scrollable = ((myTextFieldRectWithRespectToView?.origin.y)! + (myTextFieldRectWithRespectToView?.size.height)!) + 100
            }
//            let scrollable = ((myTextFieldRectWithRespectToView?.origin.y)! + (myTextFieldRectWithRespectToView?.size.height)!)
//            self.myTableView.scrollRectToVisible(keyboardBoundsWithRespectToView, animated: true)
            myTableView.contentOffset = CGPoint(x:0, y: scrollable)
            myTableView.contentInset = contentInsets
            myTableView.scrollIndicatorInsets = contentInsets
//            myTableView.scrollsToTop = false

            
        
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        let userInfo = notification.userInfo
        if let info = userInfo
        {
            let keyboardEndRectObject = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
            let keyboardBoundWithRespectToView = self.view.convertRect(keyboardEndRectObject.CGRectValue(), fromView: nil)
            let contentInsets: UIEdgeInsets
            if myVar == false {
            contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0
                , right: 0)
            } else {
            contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0
                    , right: 0)
            }
            let scrollable = (keyboardBoundWithRespectToView.origin.y) + 300
            myTableView.contentInset = contentInsets
            myTableView.scrollIndicatorInsets = contentInsets
//        self.myTableView.scrollRectToVisible(keyboardBoundWithRespectToView, animated: true)

            myTableView.contentOffset = CGPoint(x:0 , y: scrollable)
        }
    }
//    func methodForPickerViewFinished(notification: NSNotification) {
//        let userInfo = notification.userInfo
//        if let info = userInfo
//        {
//            let keyboardEndRectObject = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
//            let keyboardBoundWithRespectToView = self.view.convertRect(keyboardEndRectObject.CGRectValue(), fromView: nil)
//            let contentInsets = UIEdgeInsetsZero
//            //            let scrollable = ((myTextFieldRectWithRespectToView?.origin.y)! + ((myTextFieldRectWithRespectToView?.size.height)! + 200) + keyboardBoundWithRespectToView.origin.y)
//            myTableView.contentInset = contentInsets
//            myTableView.scrollIndicatorInsets = contentInsets
//            //            myTableView.contentOffset = CGPoint(x:0, y:scrollable)
//        }
//    }
}


    


