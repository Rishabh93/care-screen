//
//  CustomCollectionViewCell.swift
//  Care Screen
//
//  Created by Rishabh  on 15/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

public enum key : String {
    case id = "keyId"
}

class CustomCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var myButton: CustomButton!
    @IBOutlet weak var myLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func buttonClicked(sender: UIButton) {
        sender.selected = !sender.selected
//        NSUserDefaults.standardUserDefaults().setObject(myLabel.text, forKey: key.id.rawValue)
//        NSUserDefaults.standardUserDefaults().synchronize()
//        let key1 = NSUserDefaults.standardUserDefaults().objectForKey(key.id.rawValue) as! String
//        print(key1)
        
        if myButton.titleLabel?.text == "A" {
        NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier1", object: nil)
        } else if myButton.titleLabel?.text == "B" {
           NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier2", object: nil)
        } else if myButton.titleLabel?.text == "C" {
            NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier3", object: nil)
        } else if myButton.titleLabel?.text == "D" {
            NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier4", object: nil)
        } else if myButton.titleLabel?.text == "E" {
            NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier5", object: nil)
        }
    }


    override func layoutSubviews() {
        super.layoutSubviews()
        self.myButton.layer.cornerRadius = self.myButton.layer.frame.size.width / 2
    }
    
//    func updateCornerRadius() {
////        self.myButton.layer.cornerRadius = self.myButton.layer.frame.size.width / 2
////        self.myButton.layer.masksToBounds = true
////        layoutSubviews()
//    }
    
}
