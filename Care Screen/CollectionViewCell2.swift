
//
//  CollectionViewCell2.swift
//  Care Screen
//
//  Created by Rishabh  on 17/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

class CollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var myButton: UIButton!
  
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func myButtonAction(sender: UIButton) {
        if sender.selected {
//            let image1 = UIImage(named: "CheckUnselected.png")
            myImageView.image = UIImage(named: "CheckUnselected.png")
            sender.selected = false
        } else {
//            let image2 = UIImage(named: "CheckSelected.png")
            myImageView.image = UIImage(named: "CheckSelected.png")
            sender.selected = true
//            myImageView = UIImageView(image: UIImage(named: "CheckSelected.png"))
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.myButton.layer.cornerRadius = self.myButton.layer.frame.size.width / 2
    }

}

