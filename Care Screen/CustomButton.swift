//
//  CustomButton.swift
//  Care Screen
//
//  Created by Rishabh  on 15/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//       self.layer.cornerRadius = 57
//       self.layer.borderWidth = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.width / 2
    }

}

