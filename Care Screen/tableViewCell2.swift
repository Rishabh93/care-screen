//
//  CustomCell2.swift
//  Care Screen
//
//  Created by Rishabh  on 17/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit

class tableViewCell2: UITableViewCell,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var myCollectionView2: UICollectionView!
    
    @IBOutlet weak var myLabel: UILabel!
    var interSpacingBetweenCells: CGFloat!
    var collectionViewHeight: CGFloat!
    var collectionViewWidth: CGFloat!
    var maxHeightWidth: CGFloat!
    var interSpacing: CGFloat!
    var totalNumberOfCells: CGFloat!
    var cellWidth:CGFloat!
    var cellHeight:CGFloat!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let nib = UINib(nibName: "CollectionViewCell2", bundle: nil)
        
        myCollectionView2?.registerNib(nib, forCellWithReuseIdentifier: "CollectionViewCell22")
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification1:"), name: "NotificationIdentifier1", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification2:"), name: "NotificationIdentifier2", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification3:"), name: "NotificationIdentifier3", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification4:"), name: "NotificationIdentifier4", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification5:"), name: "NotificationIdentifier5", object: nil)

    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return interSpacingBetweenCells
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        self.collectionViewHeight = myCollectionView2.frame.height
        self.collectionViewWidth = myCollectionView2.frame.width
        maxHeightWidth = self.collectionViewHeight
        interSpacingBetweenCells = (collectionViewWidth * 0.2)/(totalNumberOfCells + 1)
        
        
        let totalHorizontalWidthForCells = self.collectionViewWidth * 0.8
        cellWidth = totalHorizontalWidthForCells / self.totalNumberOfCells
        cellHeight = cellWidth
        if cellHeight > collectionViewHeight {
            cellHeight = myCollectionView2.frame.height
            cellWidth = cellHeight
            interSpacingBetweenCells = (collectionViewWidth - (cellWidth! * totalNumberOfCells))/(totalNumberOfCells + 1)
            
        }
        return CGSizeMake(cellWidth!, cellHeight!)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left:interSpacingBetweenCells , bottom: 10, right:interSpacingBetweenCells)
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.totalNumberOfCells = 3
        
        
        return Int(totalNumberOfCells)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : CollectionViewCell2 = myCollectionView2.dequeueReusableCellWithReuseIdentifier("CollectionViewCell22", forIndexPath: indexPath) as! CollectionViewCell2
        cell.myButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        if indexPath.row == 0 {
            cell.myButton.setTitle("A", forState: .Normal)
                       cell.myLabel.text = "soofie"
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)

        } else if (indexPath.row == 1) {
            cell.myButton.setTitle("B", forState: .Normal)
            cell.myLabel.text = "Nepolean"
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)
            cell.myLabel.adjustsFontSizeToFitWidth = true

        } else if (indexPath.row == 2) {
            cell.myButton.setTitle("C", forState: .Normal)
            cell.myLabel.text = "Safed"
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)

        } else if (indexPath.row == 3) {
            cell.myButton.setTitle("D", forState: .Normal)
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)

        } else if (indexPath.row == 4) {
            cell.myButton.setTitle("E", forState: .Normal)
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)

        }
        return cell
    }
    func methodOfReceivedNotification1(notification: NSNotification) {
        myLabel.text = "WHO NEEDS BOARDING"
    }
    
    func methodOfReceivedNotification2(notification: NSNotification) {
        myLabel.text = "WHO NEEDS SITTING"
    }
    
    func methodOfReceivedNotification3(notification: NSNotification) {
        myLabel.text = "WHO NEEDS WALKING"
    }
    
    func methodOfReceivedNotification4(notification: NSNotification) {
        myLabel.text = "WHO NEEDS GROOMING"
    }
    
}
