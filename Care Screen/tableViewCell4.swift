//
//  tableViewCell4.swift
//  Care Screen
//
//  Created by Rishabh  on 20/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit
class tableViewCell4: UITableViewCell,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var myPickerView: UIPickerView!
    @IBOutlet weak var myTextField: UITextField!
    @IBOutlet weak var myImage: UIImageView!
    var array = ["$10","$20","$30","$40","$50","$60","$70","$80","$90","$100"]
    override func awakeFromNib() {
        super.awakeFromNib()
        myPickerView.hidden = true
        // Initialization code
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        myTextField.resignFirstResponder()
        return true
    }
    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        closeKeyboard()
//    }
    func closeKeyboard() {
        self.contentView.endEditing(true)
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return array[row]
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return array.count
    }
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        myTextField.text = array[row]
        myPickerView.hidden = true
        NSNotificationCenter.defaultCenter().postNotificationName("RowSelected", object: nil)
        myPickerView.resignFirstResponder()
    }
//    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
//        myPickerView.hidden = false
//        return true
//    }
    func textFieldDidBeginEditing(textField: UITextField) {
//        closeKeyboard()
        myPickerView.hidden = false
        NSNotificationCenter.defaultCenter().postNotificationName("ImageTapped", object: nil)
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(tableViewCell4.imageTapped(_:)))
        myImage.userInteractionEnabled = true
        myImage.addGestureRecognizer(tapGestureRecognizer)
    }
//    func textFieldDidEndEditing(textField: UITextField) {
//        NSNotificationCenter.defaultCenter().postNotificationName("RowSelected", object: nil)
//
//    }
    func imageTapped(sender: UIImageView)  {
        NSNotificationCenter.defaultCenter().postNotificationName("ImageTapped", object: nil)
        myPickerView.hidden = false
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        myPickerView.hidden = true
        return true
    }
}

