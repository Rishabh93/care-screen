//
//  tableViewCell5.swift
//  Care Screen
//
//  Created by Rishabh  on 21/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit
class tableViewCell5: UITableViewCell {
    
    @IBOutlet weak var myLabel1: UILabel!
    @IBOutlet weak var myLabel2: UILabel!
    @IBOutlet weak var myLabel3: UILabel!
    @IBOutlet weak var myImageView1: UIImageView!
    @IBOutlet weak var myImageView2: UIImageView!
    @IBOutlet weak var myImageView3: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        myLabel1.font = UIFont(name: "HelveticaNeue-UltraLight",
                               size: 15.0)
        myLabel2.font = UIFont(name: "HelveticaNeue-UltraLight",
                               size: 15.0)
        myLabel3.font = UIFont(name: "HelveticaNeue-UltraLight",
                               size: 15.0)
    }
    
    
    @IBAction func myButtonFunc1(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected == false {
            myLabel1.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)
        } else {
            myLabel1.font = UIFont(name: "Helvetica Neue",
                                       size: 15.0)
        }
    }

    @IBAction func myButtonFunc2(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected == false {
            myLabel2.font = UIFont(name: "HelveticaNeue-UltraLight",
                                   size: 15.0)
        } else {
            myLabel2.font = UIFont(name: "Helvetica Neue",
                                       size: 15.0)
        }

    }
    @IBAction func myButtonFunc3(sender: UIButton) {
        sender.selected = !sender.selected
        if sender.selected == false {
            myLabel3.font = UIFont(name: "HelveticaNeue-UltraLight",
                                   size: 15.0)
        } else {
            myLabel3.font = UIFont(name: "Helvetica Neue",
                                       size: 15.0)
        }

    }
   
}


