//
//  tableViewCell3.swift
//  Care Screen
//
//  Created by Rishabh  on 20/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit
class tableViewCell3: UITableViewCell,UITextFieldDelegate,UIPickerViewDelegate {

    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        myTextField.layer.borderColor = UIColor( red: 21/255, green: 255/255, blue:18/255, alpha: 1.0 ).CGColor
        myTextField.layer.borderWidth = 1
        myTextField.layer.cornerRadius = 15
        // Initialization code
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification1:"), name: "NotificationIdentifier1", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification2:"), name: "NotificationIdentifier2", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification3:"), name: "NotificationIdentifier3", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification4:"), name: "NotificationIdentifier4", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("methodOfReceivedNotification5:"), name: "NotificationIdentifier5", object: nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        myTextField.resignFirstResponder()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        let datePicker = UIDatePicker()
        textField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(tableViewCell3.datePickerChanged(_:)), forControlEvents: .ValueChanged)
    }
    func datePickerChanged(sender: UIDatePicker) {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MM-dd-yyyy"
       formatter.dateStyle = .LongStyle
        myTextField.text = formatter.stringFromDate(sender.date)
        closeKeyboard()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        closeKeyboard()
    }
    func closeKeyboard(){
//        NSNotificationCenter.defaultCenter().postNotificationName("DatePickerViewFinished", object: nil)
        self.contentView.endEditing(true)
    }
    func methodOfReceivedNotification1(notification: NSNotification) {
        myLabel.text = "BOARDING DATES"
    }
    
    func methodOfReceivedNotification2(notification: NSNotification) {
        myLabel.text = "SITTING DATES"
    }
    
    func methodOfReceivedNotification3(notification: NSNotification) {
        myLabel.text = "WALKING DATES"
    }
    
    func methodOfReceivedNotification4(notification: NSNotification) {
        myLabel.text = "GROOMING DATES"
    }
    
}

