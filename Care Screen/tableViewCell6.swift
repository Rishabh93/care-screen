//
//  tableViewCell6.swift
//  Care Screen
//
//  Created by Rishabh  on 21/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit
class tableViewCell6: UITableViewCell,UITextViewDelegate {
    
    @IBOutlet weak var myTextView: UITextView!

    @IBOutlet weak var placeHolderLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexSpaceBarButton =  UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        
        let doneButton =  UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(doneButtonClicked))
        doneButton.setBackButtonBackgroundVerticalPositionAdjustment(-3, forBarMetrics: .Default)
        toolbar.items = [flexSpaceBarButton, doneButton]
        myTextView.inputAccessoryView = toolbar
        
//        placeHolderLabel.numberOfLines = 0
//        placeHolderLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
    }
    
    func doneButtonClicked(){
        if(myTextView.text?.characters.count == 0)
        {
            placeHolderLabel.hidden = false
        }
        myTextView.resignFirstResponder()

    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        placeHolderLabel.hidden = true
        return true
    }
    

}
