
//
//  CustomCellTableViewCell.swift
//  Care Screen
//
//  Created by Rishabh  on 15/06/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

import UIKit
import Foundation
public enum enumName: String {
    case Sitting,Boarding,Walking,Grooming
}


class tableViewCell1: UITableViewCell,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var myCollectionView: UICollectionView!
    var screenSize: CGRect!
    var collectionViewWidth: CGFloat!
    var collectionViewHeight: CGFloat!
    var cellHeight: CGFloat?
    var cellWidth: CGFloat?
    var totalNumberOfCells : CGFloat!
    var topInset : CGFloat!
    var maxHeightWidth: CGFloat!
    var interspacing: CGFloat!
    var interSpacingBetweenCells:CGFloat!
    var obj = CustomButton()
    
    

     override func awakeFromNib() {
        super.awakeFromNib()

        self.topInset = 10;
    
        // Initialization code
//        let nib = UINib(nibName: "MyCollectionViewCell", bundle: nil)
//        collectionView?.registerNib(nib, forCellWithReuseIdentifier: "CollectionViewCell")
        let nib = UINib(nibName: "CustomCollectionViewCell", bundle: nil)
        
        myCollectionView?.registerNib(nib, forCellWithReuseIdentifier: "CollectionViewCell")

    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return interSpacingBetweenCells
    }
    
    
//    ================= INTER SPACING BETWEEN CELLS =======================
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
//      return interSpacingBetweenCells
//    }

    //================= CELL WIDTH AND HEIGHT =======================
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        self.collectionViewHeight = myCollectionView.frame.height
        self.collectionViewWidth = myCollectionView.frame.width
        maxHeightWidth = self.collectionViewHeight
        interSpacingBetweenCells = (collectionViewWidth * 0.2)/(totalNumberOfCells + 1)

        
        let totalHorizontalWidthForCells = self.collectionViewWidth * 0.8
        cellWidth = totalHorizontalWidthForCells / self.totalNumberOfCells
        cellHeight = cellWidth
        if cellHeight > collectionViewHeight {
            cellHeight = myCollectionView.frame.height
            cellWidth = cellHeight
            interSpacingBetweenCells = (collectionViewWidth - (cellWidth! * totalNumberOfCells))/(totalNumberOfCells + 1)
            
        }
        return CGSizeMake(cellWidth!, cellHeight!)
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left:interSpacingBetweenCells , bottom: 10, right:interSpacingBetweenCells)

    }

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.totalNumberOfCells = 4
        
        return Int(totalNumberOfCells)
    }

//    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//        (cell as! CustomCollectionViewCell).updateCornerRadius()
//    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : CustomCollectionViewCell = myCollectionView.dequeueReusableCellWithReuseIdentifier("CollectionViewCell", forIndexPath: indexPath) as! CustomCollectionViewCell
        cell.myButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)

        if indexPath.row == 0 {
            cell.myButton.setTitle("A", forState: .Normal)
            cell.myLabel.text = enumName.Boarding.rawValue
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                     size: 15.0)
//            cell.myLabel.adjustsFontSizeToFitWidth = true
        } else if (indexPath.row == 1) {
            cell.myButton.setTitle("B", forState: .Normal)
            cell.myLabel.text = enumName.Sitting.rawValue
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)
//            cell.myLabel.adjustsFontSizeToFitWidth = true
        } else if (indexPath.row == 2) {
        cell.myButton.setTitle("C", forState: .Normal)
            cell.myLabel.text = enumName.Walking.rawValue
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)
//            cell.myLabel.adjustsFontSizeToFitWidth = true
        } else if (indexPath.row == 3) {
            cell.myButton.setTitle("D", forState: .Normal)
            cell.myLabel.text = enumName.Grooming.rawValue
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)
//            cell.myLabel.adjustsFontSizeToFitWidth = true
        } else if (indexPath.row == 4) {
            cell.myButton.setTitle("E", forState: .Normal)
//            cell.myLabel.text = enumName.Grooming.rawValue
            cell.myLabel.font = UIFont(name: "HelveticaNeue-UltraLight",
                                       size: 15.0)
//            cell.myLabel.adjustsFontSizeToFitWidth = true
        }
        return cell
    }
}
